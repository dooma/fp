def insertAscendArr(array):
    """
    Return the object with subArrays that match the property
    x[i] < x[i+1] < ...

    Parameters:
        array (list) - the number list
        subArrays (dictionary) - the object with max parameter setted by default to 0
    Return:
        (dictionary) - the objecti with sub arrays tat match the property
    """

    # The dictionary that stores the sublists that matches the property
    subArrays = {
            "max": 0
    }


    for element in array:
        copyElement = element
        maxLength = 1
        elemIndex = array.index(element)

        for subElement in array[elemIndex + 1:]:
            if copyElement < subElement:
                copyElement = subElement
                maxLength += 1
            else:
                break

        subArrays[maxLength] = array[elemIndex : elemIndex + maxLength]

        if subArrays["max"] < maxLength:
            subArrays["max"] = maxLength

    return subArrays

def show(array):
    print(array)

def insert(array):
    while True:
        value = input("Please enter a number: ")
        try:
            value = int(value)
            array.append(value)
        except ValueError:
            print("Sorry the given string is not a number! Return...")
            break

def runTests():
    # First case
    array = [3, 20, 51, 70, 73, 145, 157, 22, 163, 179, 212, 253, 265, 1239, 1, 272, 280, 314, 346, 4, 27, 435, 458, 476]
    subArrays = insertAscendArr(array)
    print("\nList - ", array, "\nExpected result - [22, 163, 179, 212, 253, 265, 1239]")
    show(subArrays[subArrays["max"]])

    # Second case
    array = [44, 57, 78, 133, 159, 173, 182, 0, 248, 289, 293, 313, 316, 30, 328, 354, 371, 376, 404, 409, 434, 491]
    subArrays = insertAscendArr(array)
    print("\nList - ", array, "\nExpected result - [30, 328, 354, 371, 376, 404, 409, 434, 491]")
    show(subArrays[subArrays["max"]])

    # Third case
    array = [79, 88, 100, 151, 178, 186, 191, 237, 242, 292, 309, 313, 315, 321, 374, 405, 416, 460, 462, 487]
    subArrays = insertAscendArr(array)
    print("\nList - ", array, "\nExpected result - [79, 88, 100, 151, 178, 186, 191, 237, 242, 292, 309, 313, 315, 321, 374, 405, 416, 460, 462, 487]")
    show(subArrays[subArrays["max"]])

    # Fourth case
    array = [31, 65, 128, 164, 165, 171, 188, 215, 309, 319, 20, 332, 370, 401, 406, 421, 433, 449, 466, 475, 485]
    subArrays = insertAscendArr(array)
    print("\nList - ", array, "\nExpected result - [20, 332, 370, 401, 406, 421, 433, 449, 466, 475, 485]")
    show(subArrays[subArrays["max"]])

def main():
    # The list of numbers that are verified
    array = []

    menu = """
    1 - Insert into list
    2 - Show list
    3 - Get the longest sublists of elements that matches the property
    4 - Run tests
    5 - Exit\n
    """

    print("Return the longest array that has elements in ascending order")

    while True:
        command = input(menu)

        if command == "1":
            insert(array)
        elif command == "2":
            show(array)
        elif command == "3":
            subArrays = insertAscendArr(array)
            show(subArrays[subArrays["max"]])
        elif command == "4":
            runTests()
        elif command == "5":
            break

main()
