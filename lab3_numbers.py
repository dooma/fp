from collections import defaultdict

def getDigits(number):
    """
    A method that return unique digits

    Parameters:
        number (Integer) - given number to gets it's unique digits
    Returns:
        (string) - a string that contains the unique digits in descending order
    """
    return ''.join(sorted(list(set(str(number))), reverse=True))

def getSubArrays(array):
    """
    Returns a dictionary with sublists

    Parameters:
        array (list) - a list that contains the numbers
    Returns:
        (dictionary) - dictionary with lists that contains numbers with digits from attribute
    """

    container = defaultdict(list)

    for elem in array:
        digits = getDigits(elem)
        container[digits].append(elem)

    return container

def getLongestList(container):
    """
    Return the longest list from a provided dictionary

    Parameters:
        container (dictionary) - a dictionary that contains the list
    Returns:
        (list) - the longest list from dictionary
    """
    return max(container.items(), key=lambda k: len(k[1]))

def show(array):
    print(array)

def insert(array):
    while True:
        value = input("Please enter a number: ")
        try:
            value = int(value)
            array.append(value)
        except ValueError:
            print("Sorry the given string is not a number! Return...")
            break

def runTests():
    # First case
    array = [3, 20, 51, 730, 73, 145, 157, 163, 179, 212, 21111111, 211, 221, 280, 314, 341, 427, 435, 458, 476]
    subArrays = getSubArrays(array)
    print("\nList - ", array, "\nExpected result - [212, 211, 221]")
    show(getLongestList(subArrays)[1])

    # Second case
    array = [44, 57, 78, 133, 159, 173, 182, 248, 289, 293, 313, 316, 328, 354, 371, 376, 404444, 404, 440, 491]
    subArrays = getSubArrays(array)
    print("\nList - ", array, "\nExpected result - [404444, 404, 440]")
    show(getLongestList(subArrays)[1])

    # Third case
    array = [79, 88, 100, 151, 152, 186, 237, 242, 292, 3331, 313, 315, 321, 374, 405, 416, 460, 462, 487]
    subArrays = getSubArrays(array)
    print("\nList - ", array, "\nExpected result - [3331, 313]")
    show(getLongestList(subArrays)[1])

    # Fourth case
    array = [31, 65, 128, 164, 165, 171, 188, 215, 309, 319, 332, 370, 406, 406, 421, 433, 449, 4066, 475, 485]
    subArrays = getSubArrays(array)
    print("\nList - ", array, "\nExpected result - [406, 406, 4066]")
    show(getLongestList(subArrays)[1])

def main():
    array = []

    menu = """
    1 - Insert into list
    2 - Show list
    3 - Get the longest sublists of elements with same digits
    4 - Run tests
    5 - Exit\n
    """

    print("Find the longest list that numbers have same digits\nIn case of tie, it returns a random list that has found")

    while True:
        command = input(menu)

        if command == "1":
            insert(array)
        elif command == "2":
            show(array)
        elif command == "3":
            subArrays = getSubArrays(array)
            show(getLongestList(subArrays)[1])
        elif command == "4":
            runTests()
        elif command == "5":
            break

main()
