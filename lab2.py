##
# Author: Calin Salagean < salagean.calin@gmail.com >
# Lab2 homework

import re

# Read a string and strip special chars
def read():
    value = input("Please enter a number: ")
    return re.sub('[a-zA-Z]+|\W+', '', value)

number = read()

# Loop until user enter a valid string
while not number.isdigit():
    number = read()

print('The greatest number is: ', ''.join(sorted(number, reverse=True)))
